let size:number = 8,
    result: string = "";

for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
        result += (i + j) % 2 ? "◻" : "◼";
    }
    result += "\n";
}

console.log(result);

